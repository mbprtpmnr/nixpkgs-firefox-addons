# Revision history for Nixpkg Firefox Add-ons

## Version 0.8.1 (2021-08-09)

* Bump relude version lower bound, from 0.4 to 1.0.

* Bump hnix version lower bound, from 0.5 to 0.13.

* Add flake.nix file.
